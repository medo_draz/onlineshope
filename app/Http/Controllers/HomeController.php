<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index()
    {

        $products=Product::paginate(8)->sortBy('created_at');
        $categories=Category::all();
        // dd($products_slider);
        return view('welcome',compact('products','categories'));
    }

    public function product_details($id)
    {
      $product=Product::find($id);
      $categories=Category::all();
      return View('product', compact('product','categories'));
    }

    public function about()
    {
      $categories=Category::all();
      return View('about', compact('categories'));
    }

    public function contact()
    {
      $categories=Category::all();
      return View('contactus', compact('categories'));
    }

    public function cart()
    {
      $categories=Category::all();
      $user_id=Auth::user()->id;
      $carts=Cart::where('user_id',$user_id)->get();
      return View('cart', compact('categories','carts'));
    }

    public function cart_save(Request $request){
        $request->validate([
            'product_id'=>'required',
            'user_id'=>'required',
            'total_price'=>'required',
        ]);
        // dd($re?quest->all());
        $cart=new Cart();
        $cart->product_id=$request->product_id;
        $cart->user_id=Auth::user()->id;
        $cart->amount=1;
        $cart->total_price=$request->total_price;
        $cart->save();

        return response()->json([
            'status'  => true,
            'content' => $cart
        ]);
    }

    public function category($id)
    {
      $categories=Category::all();
      $products=Product::where('category_id',$id)->paginate(12);
      return View('category', compact('categories','products'));
    }

    public function product_category(Request $request)
    {
        $products=Product::where('category_id',$request->category_id)->paginate(12);
        $view=view('products_form')->with(['products'=>$products])->renderSections();

        if($products->count() > 0){
            return response()->json([
                'status'  => true,
                'content' => $view['main']
            ]);
        }
        return response()->json([
            'status'  => false,
            'content' => $view['notfound']
        ]);

    }

    public function login(){
        $categories=Category::all();
        return View('login', compact('categories'));
    }
}
