<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
          'first_name' => 'super',
          'last_name' => 'admin',
          'email' => 'super_admin@app.com',
          'phone' => '011114548624',
          'state' => '1',
          'address' => 'egypt',
          'password' => bcrypt('123456'),

        ]);

        $user->attachRole('super_admin');
    }
}
