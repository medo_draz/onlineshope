<?php

return [
    'dashboard' => 'dashboard',
    'logout' => 'logout',

    'add' => 'add',
    'create' => 'create',
    'read' => 'read',
    'edit' => 'edit',
    'update' => 'update',
    'delete' => 'delete',
    'search' => 'search',
    'show' => 'show',
    'loading' => 'loading',
    'print' => 'print',

    'confirm_delete' => 'confirm delete',
    'yes' => 'yes',
    'no' => 'no',

    'login' => 'login',
    'remember_me' => 'remember me',
    'password' => 'password',
    'password_confirmation' => 'password confirmation',

    'added_successfully' => 'added successfully',
    'updated_successfully' => 'updated successfully',
    'deleted_successfully' => 'deleted successfully',

    'no_data_found' => 'sorry no data found',
    'no_records' => 'sorry no records',

    'clients' => 'clients',
    'client_name' => 'client name',
    'phone' => 'phone',
    'address' => 'address',
    'previous_orders' => 'previous orders',
    'orders' => 'orders',
    'add_order' => 'add order',
    'edit_order' => 'edit order',

    'offers' => 'offers',
    'all_offers' => 'all offers',
    'offer' => 'offer',

    'users' => 'users',
    'first_name' => 'first name',
    'last_name' => 'last name',
    'email' => 'email',
    'image' => 'image',
    'action' => 'action',

    'permissions' => 'permissions',

    'categories' => 'categories',
    'all_categories' => 'all categories',
    'name' => 'name',
    'description' => 'description',
    'products_count' => 'products count',
    'related_products' => 'related products',
    'category' => 'category',
    'show_products' => 'show products',
    'created_at' => 'created at',

    'products' => 'products',
    'product_name' => 'product name',
    'product_id' => 'product id',
    'all_products' => 'all products',
    'product' => 'product',
    'quantity' => 'quantity',
    'total' => 'total',
    'purchase_price' => 'purchase price',
    'price' => 'price',
    'price_after_discount' => 'price after discount',
    'sale_price' => 'sale price',
    'stock' => 'quantity',
    'profit_percent' => 'profit percent',

    'ar' => [
        'name' => 'name of arabic',
        'description' => 'description of arabic',
    ],

    'en' => [
        'name' => 'name of english',
        'description' => 'description of english',
    ],

];
