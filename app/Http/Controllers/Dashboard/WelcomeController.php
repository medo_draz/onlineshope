<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Offer;
use Illuminate\Http\Request;
use App\Category;
use App\Client;
use App\Product;
use App\User;

class WelcomeController extends Controller
{
    //

    public  function index() {
        $categories_count = Category::count();
        $products_count = Product::count();
        $offers_count = Offer::count();
        $clients_count = User::where('state', '0')->count();
        $users_count = User::whereRoleIs('admin')->count();

        // dd(auth()->user()->hasPermission('read_categories'));

        return view('dashboard.welcome',compact('categories_count', 'products_count','offers_count', 'clients_count', 'users_count'));
    }
}
