@extends('layouts.app')

@section('content')

<div class="hero-wrap hero-bread" style="background-image: url('{{asset('images/bg_6.jpg')}}');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="{{ route('welcome') }}">Home</a></span> <span>Shop</span></p>
            <h1 class="mb-0 bread">Shop</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-6 mb-5 ftco-animate">
    				<a href="{{asset('uploads/product_images/'.$product->image)}}" class="image-popup prod-img-bg"><img src="{{asset('uploads/product_images/'.$product->image)}}" class="img-fluid" alt="Colorlib Template">

            @if($product->offer->offer != 0)
              <span class="status">{{$product->offer->offer}}% Off</span>
            @endif
            <div class="overlay"></div>
            </a>
    			</div>
    			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
    				<h3>{{$product->name}}</h3>
    				<div class="rating d-flex">
							<p class="text-left mr-4">
								<!-- <a href="#" class="mr-2">5.0</a> -->
								<a href="#"><span class="ion-ios-star-outline"></span></a>
								<a href="#"><span class="ion-ios-star-outline"></span></a>
								<a href="#"><span class="ion-ios-star-outline"></span></a>
								<a href="#"><span class="ion-ios-star-outline"></span></a>
								<a href="#"><span class="ion-ios-star-outline"></span></a>
							</p>
							<p class="text-left mr-4">
								<a class="mr-2" style="color: #000;">100 <span style="color: #bbb;">Rating</span></a>
							</p>
							<p class="text-left">
								<a class="mr-2" style="color: #000;">500 <span style="color: #bbb;">Sold</span></a>
							</p>
						</div>
    				<p class="price">
              @if($product->offer->offer)
              <span class="mr-2 price-dc">${{$product->sale_price}}</span>
              <span class="price-sale">${{$product->sale_price-($product->sale_price*($product->offer->offer/100))}}</span>
              @else
                <span class="price-sale">${{$product->sale_price}}</span>
              @endif
            </p>
    				<p>{!!$product->description!!}
						</p>
						<div class="row mt-4">
							<div class="col-md-6">
								<div class="form-group d-flex">
		              <div class="select-wrap">
	                  <div class="icon"><span class="ion-ios-arrow-down"></span></div>
	                  <select name="" id="" class="form-control">
	                  	<option value="">Small</option>
	                    <option value="">Medium</option>
	                    <option value="">Large</option>
	                    <option value="">Extra Large</option>
	                  </select>
	                </div>
		            </div>
							</div>
							<div class="w-100"></div>
							<div class="input-group col-md-6 d-flex mb-3">
	             	<!-- <span class="input-group-btn mr-2">
	                	<button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
	                   <i class="ion-ios-remove"></i>
	                	</button>
	            		</span> -->
	             	<input type="number" id="quantity" name="quantity" class="quantity form-control input-number" value="1" min="1" max="{{$product->quantity}}">
	             	<!-- <span class="input-group-btn ml-2">
	                	<button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
	                     <i class="ion-ios-add"></i>
	                 </button>
	             	</span>
	          	</div> -->
	          	<div class="w-100"></div>
	          	<div class="col-md-12">
	          		<p style="color: #000;">{{$product->quantity}} piece available</p>
	          	</div>
          	</div>

    			</div><p><a href="cart.html" class="btn btn-primary py-3 px-5">Buy now</a></p>
    		</div>

    	</div>
    </section>

@endsection
<script type="text/javascript">

    //change product quantity
    $('body').on('keyup change', '.product-quantity', function() {

        var quantity = Number($(this).val()); //2
        var unitPrice = parseFloat($(this).data('price').replace(/,/g, '')); //150
        console.log(unitPrice);
        $(this).closest('tr').find('.product-price').html($.number(quantity * unitPrice, 2));
        calculateTotal();

    });//end of product quantity change

    function calculateTotal() {

    var price = 0;

    $('.order-list .product-price').each(function(index) {

        price += parseFloat($(this).html().replace(/,/g, ''));

    });//end of product price

    $('.total-price').html($.number(price, 2));

    //check if price > 0
    if (price > 0) {

        $('#add-order-form-btn').removeClass('disabled')

    } else {

        $('#add-order-form-btn').addClass('disabled')

    }//end of else

    }//end of calculate total

</script>
