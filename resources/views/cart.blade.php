@extends('layouts.app')

@section('content')

 <div class="hero-wrap hero-bread" style="background-image: url('images/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="{{ route('welcome') }}">Home</a></span> <span>Cart</span></p>
            <h1 class="mb-0 bread">My Wishlist</h1>
          </div>
        </div>
      </div>
    </div>

<section class="ftco-section ftco-cart">
    <?php $subtotal=0 ?>
			<div class="container">
				<div class="row">
    			<div class="col-md-12 ftco-animate">
    				<div class="cart-list">
	    				<table class="table">
						    <thead class="thead-primary">
						      <tr class="text-center">
						        <th>&nbsp;</th>
						        <th>Image</th>
						        <th>Product</th>
						        <th>Price /$</th>
						        <th>Quantity</th>
						        <th>Total</th>
						      </tr>
						    </thead>
						    <tbody class="order-list">
                                @if ($carts->count() > 0)
                                    @foreach ($carts as $cart)
                                        <tr class="text-center">
                                            <td class="product-remove"><a href="#"><span class="ion-ios-close"></span></a></td>

                                            <td class="image-prod"><div class="img" style="background-image:url({{asset('uploads/product_images/'.$cart->product->image)}});"></div></td>

                                            <td class="product-name">
                                            <h3>{{$cart->product->name}}</h3>
                                            </td>

                                            <td class="price">{{$cart->total_price}}</td>

                                            <td class="quantity">
                                                <div class="input-group mb-3">
                                                    <input type="number" name="quantity" data-price="{{$cart->total_price}}" class="product-quantity form-control input-number" value="{{$cart->amount}}" min="1" max="{{$cart->product->quantity}}">
                                                </div>
                                            </td>

                                        <td class="total">{{$cart->total_price}}</td>
                                        <?php $subtotal += $cart->total_price ?>
                                  </tr><!-- END TR-->
                                    @endforeach

                                @else
                                  <h3>There are no products in the basket now. Please add products in your basket</h3>
                                @endif
                            </tbody>
						  </table>
					  </div>
    			</div>
    		</div>
    		<div class="row justify-content-start">
    			<div class="col col-lg-5 col-md-6 mt-5 cart-wrap ftco-animate">
    				<div class="cart-total mb-3">
    					<h3>Cart Totals</h3>
    					<p class="d-flex">
    						<span>Subtotal</span>
    						<span class="total-price">{{$subtotal}}</span>
    					</p>
    					<p class="d-flex">
    						<span>Delivery</span>
    						<span class="delivary">10.00</span>
    					</p>
    					<hr>
    					<p class="d-flex">
    						<span>Total</span>
    						<span class="total-price1">{{$subtotal+10}}</span>
    					</p>
    				</div>
    				<p class="text-center"><a href="checkout.html" class="btn btn-primary py-3 px-4">Proceed to Checkout</a></p>
    			</div>
    		</div>
			</div>
		</section>

@endsection

@section('script')

<script>
    $('body').on('keyup change','.product-quantity',function(){
        var quantity = parseInt($(this).val());
        var price = $(this).data('price');
        $(this).closest('tr').find('.total').html(quantity * price);
        claculatePrice();
    });

    function claculatePrice(){
        var totalPrice = 0;
        $('.order-list .total').each(function(){
            totalPrice += parseFloat($(this).html());
        });
        $('.total-price').html(totalPrice);
        var delivary =parseInt($('.delivary').html());
        $('.total-price1').html(totalPrice + delivary);

    }
</script>

@endsection
