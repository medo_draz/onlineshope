<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Offer;
use App\Category;
use App\Cart;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Product extends Model
{

  use Translatable;

    protected $guarded = [];
    public $translatedAttributes = ['name','description'];

    protected $appends=['image_path','profit_percent'];


  public function category()
  {
    return $this->belongsTo(Category::class);
  }

  public function offer()
  {
    return $this->belongsTo(Offer::class);
  }

//    public function cart(){
//         return $this ->  belongsTo(Cart::class);
//     }


  public function getImagePathAttribute(){
     return asset('uploads/product_images'.$this->image);
 }//end of getImagePathAttribute

 public function getProfitPercentAttribute()
     {
         $profit = $this->sale_price - $this->purchase_price;
         $profit_percent = $profit * 100 / $this->purchase_price;
         return number_format($profit_percent, 2);

     }//end of get profit attribute


}
