<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Offer extends Model
{
  protected $fillable = [
      'product_id', 'offer'];
    public function products()
    {
      return $this->hasMany(Product::class);
    }
}
