<?php

use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

define('PAGINATION','10');

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']],
    function () {

        Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function () {

            Route::get('sender', function () {
                event(new \App\Events\Chat('Hello'));
                return ['success111'];
            });

            Route::get('/', 'WelcomeController@index')->name('welcome');

            //categoy routes
            Route::resource('categories','CategoryController')->except(['show','destroy']);

            //product routes
            Route::resource('products','ProductController')->except(['show']);

            //offer routes
            Route::resource('offers','OfferController')->except(['show' ,'destroy']);

            //client routes
            Route::resource('clients','ClientController')->except(['show']);
            Route::resource('clients.orders','Client\OrderController')->except(['show']);

            //users routes
            Route::resource('users','UserController')->except(['show']);
        });

    });
