<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('welcome');

Route::get('/about', 'HomeController@about')->name('about');

Route::get('/contact', 'HomeController@contact')->name('contact');

Route::middleware(['auth'])->get('/cart', 'HomeController@cart')->name('cart');
Route::post('/cart_save', 'HomeController@cart_save')->name('cart.save');

Route::get('/category/{id}', 'HomeController@category')->name('category');

Route::get('/product_category', 'HomeController@product_category')->name('category.products');

Route::get('/product/{id}', 'HomeController@product_details')->name('product');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
