@extends('layouts.app')

@section('content')
<!-- slider secction -->
<section id="home-section" class="hero">
        <div class="home-slider owl-carousel">
            @if($products->count() > 0)
                @foreach($products as $product)
                    @if ($product->offer->offer >= 40)
                    <div class="slider-item js-fullheight">
                        <div class="overlay"></div>
                            <div class="container-fluid p-0">
                                <div class="row d-md-flex no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
                                    <img class="one-third order-md-last img-fluid" src="{{ asset('uploads/product_images/'.$product->image)}}" alt="">
                                <div class="one-forth d-flex align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                                    <div class="text">
                                    <span class="subheading">#Best Offer {{ $product->offer->offer}}%</span>
                                        <div class="horizontal">
                                        <h1 class="mb-4 mt-3">{{ $product->name }}</h1>
                                        <p class="mb-4">{!! $product->description !!}</p>
                                            <p><a href="{{ route('product',$product->id) }}" class="btn-custom">Discover Now</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
                @endforeach
            @else

                <div class="slider-item js-fullheight">
                <div class="overlay"></div>
                <div class="container-fluid p-0">
                    <div class="row d-md-flex no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
                    <img class="one-third order-md-last img-fluid" src="{{ asset('images/bg_1.png')}}" alt="">
                        <div class="one-forth d-flex align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                        <div class="text">
                            <span class="subheading">#New Arrival</span>
                            <div class="horizontal">
                                <h1 class="mb-4 mt-3">Shoes Collection 2019</h1>
                                <p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country.</p>

                                <p><a href="#" class="btn-custom">Discover Now</a></p>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>
                </div>

                <div class="slider-item js-fullheight">
                <div class="overlay"></div>
                <div class="container-fluid p-0">
                    <div class="row d-flex no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
                    <img class="one-third order-md-last img-fluid" src="{{ asset('images/bg_2.png')}}" alt="">
                        <div class="one-forth d-flex align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                        <div class="text">
                            <span class="subheading">#New Arrival</span>
                            <div class="horizontal">
                                <h1 class="mb-4 mt-3">New Shoes Winter Collection</h1>
                                <p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country.</p>

                                <p><a href="#" class="btn-custom">Discover Now</a></p>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
            @endif
        </div>
    </section>
<!--end slider secction -->

<!-- new arrivel section -->
 <section class="ftco-section bg-light">
        <div class="container">
                <div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">New Shoes Arrival</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
          </div>
        </div>
        </div>
        <div class="container">
            <div class="row">
              @foreach ($products as $product)
                <div class="col-sm-12 col-md-6 col-lg-3 ftco-animate d-flex">
                    <div class="product d-flex flex-column">
                        <a href="{{ route('product',$product->id) }}" class="img-prod"><img class="img-fluid" src="{{ asset('uploads/product_images/'.$product->image) }}" alt="product image">
                            <p id="product_id" style="visibility: hidden;">{{$product->id}}</p>
                          @if($product->offer->offer != 0)
                            <span class="status">{{$product->offer->offer}}% Off</span>
                          @endif



                          <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3">
                            <div class="d-flex">
                                <div class="cat">
                                    <span>{{$product->category->name}}</span>
                                </div>
                                <div class="rating">
                                    <p class="text-right mb-0">
                                        <a href="#"><span class="ion-ios-star-outline"></span></a>
                                        <a href="#"><span class="ion-ios-star-outline"></span></a>
                                        <a href="#"><span class="ion-ios-star-outline"></span></a>
                                        <a href="#"><span class="ion-ios-star-outline"></span></a>
                                        <a href="#"><span class="ion-ios-star-outline"></span></a>
                                    </p>
                                </div>
                            </div>
                            <h3><a href="{{ route('product',$product->id) }}" id="product_name">{{$product->name}}</a></h3>
                            <div class="pricing">
                                <p class="price">
                                  <!-- <span>{{$product->offer->offer}}</span> -->
                                  @if($product->offer->offer)
                                  <span class="mr-2 price-dc">${{$product->sale_price}}</span>
                                  <span id="total_price" class="price-sale">${{$product->sale_price-($product->sale_price*($product->offer->offer/100))}}</span>
                                  @else
                                    <span id="total_price">${{$product->sale_price}}</span>
                                  @endif
                                </p>
                            </div>
                            <p class="bottom-area d-flex px-3">
                                @if(auth()->user())
                                    @if($product->offer->offer)
                                    <?php $price=$product->sale_price-($product->sale_price*($product->offer->offer/100)) ?>
                                        <button  onclick="cart({{$product->id}},{{$price}})" class="buy buy-now text-center py-2">Buy now<span><i class="ion-ios-cart ml-1"></i></span></button>
                                    @else
                                        <button  onclick="cart({{$product->id}},{{$product->price}})" class="buy buy-now text-center py-2">Buy now<span><i class="ion-ios-cart ml-1"></i></span></button>
                                    @endif
                                @else
                                    <button  class="disabled buy buy-now text-center py-2 ">Buy now<span><i class="ion-ios-cart ml-1"></i></span></button>
                                @endif

                                {{-- <a href="#" onclick="cart({{$product->id}})" class="buy-now text-center py-2">Buy now<span><i class="ion-ios-cart ml-1"></i></span></a> --}}
                            </p>
                        </div>
                    </div>
                </div>
              @endforeach

            </div>
        </div>
    </section>

<!-- Category section -->
<section class="ftco-section ftco-choose ftco-no-pb ftco-no-pt">
        <div class="container">
            <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Categories</h2>
          </div>
                <div class="row no-gutters">
                    <div class="col-lg-4">
                        <div class="choose-wrap divider-one img p-5 d-flex align-items-end" style="background-image: url({{ asset('images/choose-1.jpg')}});">

                        <div class="text text-center text-white px-2">
                                <span class="subheading">Men's Shoes</span>
                            <h2>Men's Collection</h2>
                            <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                            <p><a href="#" class="btn btn-black px-3 py-2">Shop now</a></p>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-8">
                    <div class="row no-gutters choose-wrap divider-two align-items-stretch">
                        <div class="col-md-12">
                            <div class="choose-wrap full-wrap img align-self-stretch d-flex align-item-center justify-content-end" style="background-image: url({{ asset('images/choose-2.jpg')}});">
                                <div class="col-md-7 d-flex align-items-center">
                                    <div class="text text-white px-5">
                                        <span class="subheading">Women's Shoes</span>
                                        <h2>Women's Collection</h2>
                                        <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                                        <p><a href="#" class="btn btn-black px-3 py-2">Shop now</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row no-gutters">
                                <div class="col-md-6">
                                    <div class="choose-wrap wrap img align-self-stretch bg-light d-flex align-items-center">
                                        <div class="text text-center px-5">
                                            <span class="subheading">Summer Sale</span>
                                            <h2>Extra 50% Off</h2>
                                            <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                                            <p><a href="#" class="btn btn-black px-3 py-2">Shop now</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="choose-wrap wrap img align-self-stretch d-flex align-items-center" style="background-image: url({{ asset('images/choose-3.jpg')}});">
                                        <div class="text text-center text-white px-5">
                                            <span class="subheading">Shoes</span>
                                            <h2>Best Sellers</h2>
                                            <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                                            <p><a href="#" id="cart" class="btn btn-black px-3 py-2">Shop now</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- end category section -->

<!--  -->
<section class="ftco-gallery">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 heading-section text-center mb-4 ftco-animate">
            <h2 class="mb-4">Follow Us On Instagram</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
          </div>
            </div>
        </div>
        <div class="container-fluid px-0">
            <div class="row no-gutters">
                    <div class="col-md-4 col-lg-2 ftco-animate">
                        <a href="images/gallery-1.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url(images/gallery-1.jpg);">
                            <div class="icon mb-4 d-flex align-items-center justify-content-center">
                            <span class="icon-instagram"></span>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-lg-2 ftco-animate">
                        <a href="images/gallery-2.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url(images/gallery-2.jpg);">
                            <div class="icon mb-4 d-flex align-items-center justify-content-center">
                            <span class="icon-instagram"></span>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-lg-2 ftco-animate">
                        <a href="images/gallery-3.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url(images/gallery-3.jpg);">
                            <div class="icon mb-4 d-flex align-items-center justify-content-center">
                            <span class="icon-instagram"></span>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-lg-2 ftco-animate">
                        <a href="images/gallery-4.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url(images/gallery-4.jpg);">
                            <div class="icon mb-4 d-flex align-items-center justify-content-center">
                            <span class="icon-instagram"></span>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-lg-2 ftco-animate">
                        <a href="images/gallery-5.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url(images/gallery-5.jpg);">
                            <div class="icon mb-4 d-flex align-items-center justify-content-center">
                            <span class="icon-instagram"></span>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-lg-2 ftco-animate">
                        <a href="images/gallery-6.jpg" class="gallery image-popup img d-flex align-items-center" style="background-image: url(images/gallery-6.jpg);">
                            <div class="icon mb-4 d-flex align-items-center justify-content-center">
                            <span class="icon-instagram"></span>
                        </div>
                        </a>
                    </div>
        </div>
        </div>
    </section>
    <!-- -->

@endsection

@section('script')

    <script>
        function cart(pro_id,price){
            $.ajax({
                type  :'post',
                url   :'{{route('cart.save')}}',
                data  : {
                    '_token': "{{csrf_token()}}",
                    product_id : pro_id,
                    total_price : price,
                    // amount : $('#amount').text(),
                },success :function(data){
                    // if(data.status == true){
                    //     $('#products').empty().html(data.content);
                    // }else{

                    // }
                }, error: function (reject) {
                }
            });
        }
        // $(document).on('click','#cart',function(e){
        //     e.preventDefault();
        //     $.ajax({
        //         type  :'post',
        //         url   :'{{route('cart.save')}}',
        //         data  : {
        //             '_token': "{{csrf_token()}}",
        //             product_id : $('#product_id').text(),
        //             total_price : $('#total_price').text(),
        //             amount : $('#amount').text(),
        //         },success :function(data){
        //             // if(data.status == true){
        //             //     $('#products').empty().html(data.content);
        //             // }else{

        //             // }
        //         }, error: function (reject) {
        //         }
        //     });
        // });

    </script>

@endsection
