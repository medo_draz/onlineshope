<?php

namespace App;
use App\Product;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = "carts";
    protected $fillable=['product_id','user_id','amount' ,'total_price','created_at','updated_at'];
    protected $hidden =['created_at','updated_at'];
    public $timestamps = true;

    public function product(){
        return $this ->  belongsTo(Product::class,'product_id','id');
    }
}
