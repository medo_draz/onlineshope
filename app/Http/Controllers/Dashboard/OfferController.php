<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Offer;
use App\Product;
use Illuminate\Http\Request;

class OfferController extends Controller
{

    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_offers'])->only('index');
        $this->middleware(['permission:create_offers'])->only('create');
        $this->middleware(['permission:update_offers'])->only('edit');

    }//end of constructor

    public function index(Request $request)
    {
        $products=Product::all();
        $offers=Offer::when($request->search,function ($q) use ($request) {

            return $q->where('offer', 'like', '%' . $request->search . '%');

        })->when($request->product_id, function ($q) use ($request) {

            return $q->where('product_id', $request->product_id);

        })->latest()->paginate(PAGINATION);
        return view('dashboard.offers.index',compact('offers','products'));
    }

    public function create()
    {
        $products=Product::all();
        return view('dashboard.offers.create',compact('products'));
    }

    public function store(Request $request)
    {
      $request->validate([
          'offer'=>'required|unique:offers',
      ]);
      Offer::create($request->all());
      session()->flash('success', __('site.added_successfully'));
      return redirect()->route('dashboard.offers.index');
    }

    public function edit(Offer $offer)
    {
        return view('dashboard.offers.edit',compact('offer'));
    }

    public function update(Request $request, Offer $offer)
    {
      $request->validate([
          'offer'=>'required|unique:offers',
      ]);
      $offer->update($request->all());
      session()->flash('success', __('site.updated_successfully'));
      return redirect()->route('dashboard.offers.index');
    }

}
