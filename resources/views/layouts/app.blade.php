<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Online Shop</title>
    <link href="images/shopify-bag.ico" rel="shortcut icon"/>

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/open-iconic-bootstrap.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }} ">

    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }} ">

    <link rel="stylesheet" href="{{ asset('css/aos.css') }} ">

    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.css') }} ">


    <link rel="stylesheet" href="{{ asset('css/flaticon.css') }} ">
    <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }} ">
</head>
<body class="goto-here">

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ route('welcome') }}">OnlineShop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="{{ route('welcome') }}" class="nav-link">Home</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Categories</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        @if ($categories)
                            @foreach ($categories as $category)
                                <a class="dropdown-item" href="{{ route('category',$category->id) }}">{{ $category->name }}</a>
                        @endforeach
                    @endif

                     {{-- <a class="dropdown-item" href="shop.html">Men's Cloths</a>
                    <a class="dropdown-item" href="product-single.html">Women's Cloths</a>
                    <a class="dropdown-item" href="cart.html">Childern's Cloths</a> --}}
                    <!-- <a class="dropdown-item" href="checkout.html">Checkout</a> -->
                    </div>
                </li>
                <li class="nav-item"><a href="{{ route('about') }}" class="nav-link">About</a></li>
                {{-- <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li> --}}
                <li class="nav-item"><a href="{{ route('contact') }}" class="nav-link">Contact</a></li>
                <li class="nav-item cta cta-colored"><a href="{{ route('cart') }}" class="nav-link"><span
                            class="icon-shopping_cart"></span>[0]</a></li>

                @if(auth()->user())
                    {{--<!-- User Account: style can be found in dropdown.less -->--}}
                    <li class="dropdown user user-menu">

                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ asset('uploads/user_images/'.auth()->user()->image) }}"
                                 style="  border-radius:50%; width: 50px; height: auto;" class="user-image"
                                 alt="User Image">
                            <span
                                class="hidden-xs">{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}</span>

                        </a>
                        <ul class="dropdown-menu">

                            {{--<!-- User image -->--}}
                            <li class="user-header text-center">
                                <img src="{{ asset('uploads/user_images/'.auth()->user()->image) }}"
                                     style="  border-radius:50%; width: 50px; height: auto;" class="img-circle"
                                     alt="User Image">

                                <p>
                                    {{ auth()->user()->first_name }} {{ auth()->user()->last_name }}<br>
                                    <small>Member since 2 days</small>
                                </p>
                            </li>

                            {{--<!-- Menu Footer-->--}}
                            <li class="user-footer text-center">


                                <a href="{{ route('logout') }}" class="btn btn-primary btn-flat" onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">@lang('site.logout')</a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                            </li>
                        </ul>
                    </li>

                @else
                    <li class="nav-item"><a href="{{ route('login') }}" class="nav-link">Sign in</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->

@yield('content')


<!-- footer -->

<footer class="ftco-footer ftco-section">
    <div class="container">
        <div class="row">
            <div class="mouse">
                <a href="#" class="mouse-icon">
                    <div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
                </a>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Online Shop</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li class="ftco-animate"><a class="twitter" href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a class="facebook" href="#">
                                <img src="{{asset('images/facebook.png')}}"
                                     style="width: 50px; height: 50px; border-radius: 50%;"/>
                            </a></li>
                        <li class="ftco-animate"><a class="instagram" href="#">
                                <img src="{{asset('images/instagram.png')}}"
                                     style="width: 50px; height: 50px; border-radius: 50%;"/>
                            </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4 ml-md-5">
                    <h2 class="ftco-heading-2">Menu</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block">Shop</a></li>
                        <li><a href="#" class="py-2 d-block">About</a></li>
                        <li><a href="#" class="py-2 d-block">Journal</a></li>
                        <li><a href="#" class="py-2 d-block">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Help</h2>
                    <div class="d-flex">
                        <ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
                            <li><a href="#" class="py-2 d-block">Shipping Information</a></li>
                            <li><a href="#" class="py-2 d-block">Returns &amp; Exchange</a></li>
                            <li><a href="#" class="py-2 d-block">Terms &amp; Conditions</a></li>
                            <li><a href="#" class="py-2 d-block">Privacy Policy</a></li>
                        </ul>
                        <ul class="list-unstyled">
                            <li><a href="#" class="py-2 d-block">FAQs</a></li>
                            <li><a href="#" class="py-2 d-block">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Have a Questions?</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span>
                            </li>
                            <li><a href="#"><span class="icon icon-phone"></span><span
                                        class="text">+2 392 3929 210</span></a></li>
                            <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@yourdomain.com</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                    All rights reserved | This template is made with <i class="icon-heart color-danger"
                                                                        aria-hidden="true"></i> by <a href="#"
                                                                                                      target="_blank">Online
                        Shop</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            </div>
        </div>
    </div>
</footer>

<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00"/>
    </svg>
</div>


<script src="{{ asset('js/jquery.min.js') }} "></script>
<script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }} "></script>
<script src="{{ asset('js/popper.min.js') }} "></script>
<script src="{{ asset('js/bootstrap.min.js') }} "></script>
<script src="{{ asset('js/jquery.easing.1.3.js') }} "></script>
<script src="{{ asset('js/jquery.waypoints.min.js') }} "></script>
<script src="{{ asset('js/jquery.stellar.min.js') }} "></script>
<script src="{{ asset('js/owl.carousel.min.js') }} "></script>
<script src="{{ asset('js/jquery.magnific-popup.min.js') }} "></script>
<script src="{{ asset('js/aos.js') }} "></script>
<script src="{{ asset('js/jquery.animateNumber.min.js') }} "></script>
<script src="{{ asset('js/bootstrap-datepicker.js') }} "></script>
<script src="{{ asset('js/scrollax.min.js') }} "></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="{{ asset('js/google-map.js') }} "></script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/main.js') }} "></script>
@yield('script')
</body>
</html>
